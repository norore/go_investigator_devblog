#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Norore'
SITENAME = 'GO Investigator devblog'
SITESUBTITLE = 'A simple devblog on a not so simple bioinformatic field!'
SITEURL = '.'

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'fr'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Dépôt du projet', 'https://git.imirhil.fr/norore/go_investigator'),
         ('Norore', 'https://norore.fr/'),
         ('Bioinfo-fr', 'https://bioinfo-fr.net'))

# Social widget
SOCIAL = (('Twitter', 'https://twitter.com/nlavielle'),
          ('Mastodon', 'https://mamot.fr/@nlavielle'))

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

DISPLAY_PAGES_ON_MENU = False

D_EXTENSIONS = ['codehilite(noclasses=True, pygments_style=native)', 'extra']
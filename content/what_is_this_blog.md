Title: What is this blog?
Date: 06-02-2020 18:00
Slug: cest_quoi_ce_blog
Summary: A short presentation of the tiny project idea that have grown in my fertile brain, a day of post-lockdown, still in remote work.
Category: devblog
Tags: presentation
Lang: en
Translation: true

Good question, thanks to ask it!

Bad joke apart, this blog is a development blog on a project idea that came to me after a chat on IRC, on the french 
canal #bioinfo-fr from [freenode.org](https://freenode.org) server.

# The start point

As I started to think about this project while forgoting to take note from where this idea came, I asked a friend if she 
could refresh my memory.

> The idea that probably inspired you was me grumbling that there is no good GO network terms viewer, other than 
> QuickGo. Basically, a viewer where you can choose to "run through" one node of the ontology to be able to see next
> links. Currently I know viewer only on two extrems:
>
>  * QuickGO where you see 2 child GO terms max (2 level of links so) and then you have to reload page to see the 2 
> next while loosing parents.
>
> * Generalists ontology tools where you load the totality on the ontology in viewing. But GO is very huge and you not 
> always have resources to be able to do this. And if you can do this, it took a long time.

So, it seems that some scientits have notive that it is difficult, with current tools, for a gene ontology in particular,
to know its adjacents ontologies (parents and childs). This could look unimportant, nay negligible as constraint, but 
when we work, for example, on gene networks, particularly in exploratory phase, it could be interesting to see if the 
observed ontology is linked to an ajdactent ontology, and this, in view manner.

# The project

I was looking for a good project idea in free (as libre) scientifique development, from a long time, and that could 
answer to a real need. Also, [Python](https://www.python.org/) programing language miss me sometimes, and I really 
appreciated to play with graphs during my biocomputing studies. More of that, ontologies are a large and complete study 
domain, and they are really interesting from the point of view of informations they bring. In general manner, I really 
like playing with data: how to extract them, how to show them, how to make data talk, how to link them together…

The aim will be to offer a different viewer solution from those that exists, based on problems raised from 
scientists colleagues.

# Which mean(s)?

As I already said, Python miss me. So this project should start by using Python, I will see during the process, and my 
motivation, if I can manage it.

In this blog, you will find advanced on the project — spoil: to the time I am writing those lines, none code line had 
already been written, sorry not sorry — but also my reflexions and the points where I am stuck in. Yes, wanted to draw 
graphs, it is a good thing, but the [graph theory](https://en.wikipedia.org/wiki/Graph_theory), it a little complex 
(maths 😱!), and it is from 2009 for me, about. Let me tell you that it will not be necessarily simple, and there is a 
risk to have also bibliography and research! I will share it with you.

Scientists that are working on genetics are not all at ease with command line, a graphical part is conceivable, that 
add a starting difficulty, but it will be more stimulating.

Off course, the ideal will be to offer this to scintist community, the tool that is still in my mind will be under a 
free and open source licence. I am still not sure for the licence, but 
[AGPL](https://en.wikipedia.org/wiki/Affero_General_Public_License) seems appropriated.

# But what is an ontology?

Ontology is a term well known and used in information science. It is a way to describe an object or a concept in 
particular. We can encountered it in differents fields. That interest us for this project, it is the public data 
provided by the [Gene Ontology Consortium](http://geneontology.org/).

  * How are described the genes?
  * How can we link them each other?
  * Which kind of search could be made on this data?
  * How to extract information of interest?
  * What there is in that could be managed?

This is the kind of questions that I like to explore. Thanks to the scientists from the french community of 
bioinformatics that gave me this great motivation!

# Last point

Before leaving each other, I want to precise that this project is still in nascent stage and can only be doing on my 
free time, I already have a full time job. So it will progress in function of my tired status and my motivation, while 
continuing my other activities, still on my free time.

Good, and if I started?
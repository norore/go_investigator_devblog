Title: C’est quoi ce blog ?
Date: 06-02-2020 18:00
Slug: cest_quoi_ce_blog
Summary: Une petite présentation de l’embryon d’idée de projet qui a germé dans mon cerveau fertile, un jour de post-confinement mais toujours en télétravail.
Category: devblog
Tags: presentation
Lang: fr
Translation: true

Très bonne question, merci de l’avoir posée !

Plaisanterie de mauvais goût mise à part, ce blog est un blog de développement sur une idée de projet qui m’est venue 
suite à une conversation sur IRC, sur le canal #bioinfo-fr du serveur [freenode.org](https://freenode.org).

# Le point de départ

Comme j’ai commencé à réfléchir à ce projet tout en oubliant de noter d’où m’en était venue l’idée, j’ai demandé à une 
amie si elle pouvait me rafraîchir la mémoire.

> L'idée qui a dû t’inspirer c’était moi qui ronchonnait qu’il n'existait pas de bon visualiseur de termes GO en 
> réseau autre que QuickGO. En gros, que tu puisses choisir de "dérouler" un nœud de l’ontologie pour pouvoir voir 
> les liens suivants. Actuellement je ne connais de visualiseur que dans les deux extrêmes :
>
>   * Quick GO où tu vois max 2 GO term enfants (2 niveaux de lien donc) et ensuite faut recharger la page pour voir 
> les 2 suivants tout en perdant les parents
>
>   * Les outils d'ontologie généralistes où ils te balancent la totalité de l'ontologie en visuel. Sauf que GO c'est 
> super gros et t'as pas toujours les ressources pour y arriver. Et quand bien meme t'y arrive, ça met des plombes.

Ainsi donc, il semblerait que des scientifiques aient constaté qu’il est difficile, avec les outils actuels, 
pour une ontologie de gène en particulier, de connaître ses ontologies adjacentes (parents et enfants). Cela peut 
sembler anodin, voire minime comme contrainte, mais lorsque l’on travaille, par exemple, sur des réseaux de gènes, 
notamment en phase exploratoire, il peut être intéressant de voir si l’ontologie observée est liée à une ontologie 
voisine, et ce, de manière visuelle.

# Le projet

Cela faisait longtemps que je cherchais une idée de projet de développement libre, scientifique, et qui puisse répondre 
à un besoin réel. Il se trouve que le langage [Python](https://www.python.org/) me manque parfois, et que j’avais bien 
aimé jouer avec les graphes au cours de mes études de bioinformatique. En plus de cela, les ontologies sont un domaine 
d’étude à la fois vaste et complet, tout en étant très intéressant d’un point de vue de l’information que cela apporte. 
De manière générale, j’aime beaucoup jouer avec les données : comment les extraire, comment les afficher, comment les 
faire parler, comment les lier entre elles…

Le but sera donc de proposer une solution de visualisation différente de celles existantes, en se basant sur les 
problèmes soulevés par les collègues scientifiques.

# Quel(s) moyen(s) ?

Comme je l’ai déjà dit, Python me manque. Aussi le projet devrait commencer par du Python, à voir au cours du temps, et 
de ma motivation, si je parviens à mes fins.

Dans ce blog, vous trouverez donc des avancées sur le projet — spoil : à l’heure où j’écris ces lignes, aucune ligne de 
code n’a encore été écrite, déso pas déso — mais aussi mes réflexions et les points sur lesquels je bloque. Ben oui, 
vouloir faire des graphes, c’est bien joli, mais la 
[théorie des graphes](https://fr.wikipedia.org/wiki/Th%C3%A9orie_des_graphes), c’est un poil complexe (des maths 😱 !) 
et ça remonte à 2009 pour moi, environ. Autant vous dire que ce ne sera pas forcément simple, et qu’il risque 
donc d’y avoir aussi de la bibliographie et un peu de recherche ! Que je vous partagerai.

Les scientifiques travaillant sur la génétique n’étant pas tous très à l’aise avec la ligne de commande, une partie 
graphique est envisageable, ce qui rajoute une difficulté de départ, mais n’en n’est pas moins stimulant.

Bien sûr, comme l’idéal ce sera de le rendre disponible pour la communauté scientifique, l’outil que j’ai en tête sera 
sous une license libre et gratuite. Je ne me suis pas encore arrêtée complétement sur la licence, mais 
l’[AGPL](https://en.wikipedia.org/wiki/Affero_General_Public_License) me semble appropriée.

# Mais c’est quoi une ontologie ?

L’ontologie est un terme bien connu et utilisé en science de l’information. C’est un moyen de décrire un objet ou un 
concept en particulier. On peut en retrouver dans différents domaines. Ce qui nous intéresse pour ce projet, ce sont 
les données publiques fournies par le [Consortium Gene Ontology](http://geneontology.org/).

  * Comment sont décrits les gènes ?
  * Comment peut-on les lier entre eux ?
  * Quel type de recherche pourrait être fait sur ces données ?
  * Comment en extraire de l’information utile ?
  * Qu’est-ce qu’il y a dedans qui pourrait être exploité ?
  
 C’est ce genre de question que j’aime bien explorer. Merci aux scientifiques de la communauté francophone de 
 bioinformatique de me donner cette super motivation !

# Un dernier point

Avant de se quitter, je tiens à préciser que ce projet est encore au stade embryonnaire et ne peut se faire que sur mon 
temps libre, ayant un travail à temps plein. Aussi il avancera en fonction de mon état de fatigue et de ma motivation, 
tout en continuant mes autres activités, toujours sur mon temps libre.

Bon, et si je m’y mettais ?